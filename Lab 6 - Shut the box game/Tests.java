public class Tests {
    public static void main(String[] args) {
        Board boardA = new Board();
        myAssertUtil.setPrivateField(boardA, "closedTiles", new boolean[]{false, false, false, true, false, true, false, false, false, false, true, false});
        myAssertUtil.assert_String(boardA,
                "1 2 3 X 5 X 7 8 9 10 X 12");
        Board boardB = new Board();
        myAssertUtil.setPrivateField(boardB, "closedTiles", new boolean[]{false, false, false, true, false, true, false, false, false, false, true, false});
        myAssertUtil.assert_String(boardB,
                "1 2 3 X 5 X 7 8 9 10 X 13");
        System.out.println(123);

        Board2 board2_A = new Board2();
        myAssertUtil.setPrivateField(board2_A, "closedTiles", new boolean[]{false, false, false, true, false, true, false, false, false, false, true, false});
        myAssertUtil.assert_String(board2_A,
                "1 2 3 X 5 X 7 8 9 10 X 12");
        Board2 board2_B = new Board2();
        myAssertUtil.setPrivateField(board2_B, "closedTiles", new boolean[]{false, false, false, true, false, true, false, false, false, false, true, false});
        myAssertUtil.assert_String(board2_B,
                "1 2 3 X 5 X 7 8 9 10 X 13");
        System.out.println(234);
    }

}
