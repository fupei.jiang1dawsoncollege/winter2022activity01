import java.lang.reflect.Field;

public class Board2 {
    private Die die1;
    private Die die2;

    private boolean[] closedTiles; //{1-12,...}

    // https://stackoverflow.com/questions/8894258/fastest-way-to-iterate-over-all-the-chars-in-a-string#11876086
    // https://www.javaspecialists.eu/archive/Issue237-String-Compaction.html#:~:text=catch%20(NoSuchFieldException%20e)%20%7B%0A%20%20%20%20%20%20throw%20new%20ExceptionInInitializerError(e)%3B%0A%20%20%20%20%7D
    private static final Field strValue_bytes_field;
    static {
        try {
            strValue_bytes_field = String.class.getDeclaredField("value");
            strValue_bytes_field.setAccessible(true);
        } catch (NoSuchFieldException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    public Board2() {
        this.die1 = new Die();
        this.die2 = new Die();

        this.closedTiles = new boolean[12];
    }

    /* public String toString() {
        // 1 2 3 X 5 X 7 8 9 10 X 12
        String s = this.closedTiles[0] ? "X" : "1";
        for (int i = 1; i < this.closedTiles.length; i++) {
            s += " " + (this.closedTiles[i] ? "X" : Integer.toString(i + 1));
        }
        return s;
    } */

    /* public String toString() {
        // 1 2 3 X 5 X 7 8 9 10 X 12
        String[] arrToJoin = new String[this.closedTiles.length];
        for (int i = 0; i < this.closedTiles.length; i++) {
            if (this.closedTiles[i]) {
                arrToJoin[i] = "X";
            } else {
                arrToJoin[i] = Integer.toString(i + 1);
            }
        }
        return String.join(" ", arrToJoin);
    } */

    public String toString() {
        // 1 2 3 X 5 X 7 8 9 10 X 12
        // it's not fixed length because 12 can become X
        // try {
        //counting
        int size = this.closedTiles.length - 1;
        for (int i = 0; i < this.closedTiles.length; i++) {
            if (this.closedTiles[i]) {
                size++;
            } else {
                size += digitsInInt(i + 1);
            }
        }
        //filling
        final char[] buf = new char[size];
        if (this.closedTiles[0]) {
            buf[0] = 'X';
        } else {
            buf[0] = '1';
        }
        int pos = 1;
        for (int i = 1; i < this.closedTiles.length; i++) {
            buf[pos] = ' ';
            pos++;
            if (this.closedTiles[i]) {
                buf[pos] = 'X';
                pos++;
            } else {
                /// according to benchmark, charAt is actually faster for small
                /// assume 1 character, or 2.. {10, 11, 12}
                String str_int = Integer.toString(i + 1);
                // final char[] chars = (char[]) strValue_bytes_field.get(str_int);
                // final int len = chars.length;
                // for (int j = 0; j < len; j++) {
                //     buf[pos + j] = chars[j];
                // }
                // pos += len;

                /// so I use charAt()
                for (int j = 0; j < str_int.length(); j++) {
                    buf[pos + j] = str_int.charAt(j);
                }
                pos += str_int.length();

            }
        }
        return new String(buf);
        // } catch (IllegalAccessException e) {
        //     throw new RuntimeException(e);
        // }
        //I don't know how to benchmark..
    }

    public boolean playATurn() {
        this.die1.roll();
        this.die2.roll();
        System.out.println(this.die1 + ", " + this.die2);
        int sum = this.die1.getPips() + this.die2.getPips();
        if (this.closedTiles[sum - 1]) {
            System.out.println("Tile " + sum + " is Already Shut!");
            return true;
        } else {
            this.closedTiles[sum - 1] = true;
            System.out.println("Closing tile: " + sum);
            return false;
        }
    }

    // https://stackoverflow.com/questions/1306727/way-to-get-number-of-digits-in-an-int#1308407
    // this is below Integer.MAX_VALUE
    private static int digitsInInt(int n) {
        if (n < 100000) {
            // 5 or less
            if (n < 100) {
                // 1 or 2
                if (n < 10)
                    return 1;
                else
                    return 2;
            } else {
                // 3 or 4 or 5
                if (n < 1000)
                    return 3;
                else {
                    // 4 or 5
                    if (n < 10000)
                        return 4;
                    else
                        return 5;
                }
            }
        } else {
            // 6 or more
            if (n < 10000000) {
                // 6 or 7
                if (n < 1000000)
                    return 6;
                else
                    return 7;
            } else {
                // 8 to 10
                if (n < 100000000)
                    return 8;
                else {
                    // 9 or 10
                    if (n < 1000000000)
                        return 9;
                    else
                        return 10;
                }
            }
        }
    }

}
