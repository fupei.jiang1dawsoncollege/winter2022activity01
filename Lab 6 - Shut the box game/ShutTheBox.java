public class ShutTheBox {
    public static void main(String[] args) {
        System.out.println("Welcome to ShutTheBox!");
        Board board_ = new Board();
        while (true) {
            System.out.println("Player 1's turn");
            System.out.println(board_);
            if (board_.playATurn()) {
                System.out.println("Player 2 wins");
                break;
            }

            System.out.println("Player 2's turn");
            System.out.println(board_);
            if (board_.playATurn()) {
                System.out.println("Player 1 wins");
                break;
            }
        }

    }
}
