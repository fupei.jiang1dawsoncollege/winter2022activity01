public class ShutTheBox2 {
    public static void main(String[] args) {
        System.out.println("Welcome to ShutTheBox!");

        int player1WinsCount = 0;
        int player2WinsCount = 0;

        while (true) {
            Board board_ = new Board();
            while (true) {
                System.out.println("Player 1's turn");
                System.out.println(board_);
                if (board_.playATurn()) {
                    System.out.println("Player 2 wins");
                    player2WinsCount++;
                    break;
                }

                System.out.println("Player 2's turn");
                System.out.println(board_);
                if (board_.playATurn()) {
                    System.out.println("Player 1 wins");
                    player1WinsCount++;
                    break;
                }
            }
            if (MyInputUtil.inputY_N_withDefault("do you wish to continue ? (Y/n)", true)) {
                continue;
            }
            break;
        }

        System.out.println("Player1 won " + player1WinsCount + (player1WinsCount == 1 ? " time" : " times"));
        System.out.println("Player2 won " + player2WinsCount + (player2WinsCount == 1 ? " time" : " times"));

    }
}
