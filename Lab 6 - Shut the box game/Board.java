public class Board {
    private Die die1;
    private Die die2;

    private boolean[] closedTiles; //{1-12,...}

    public Board() {
        this.die1 = new Die();
        this.die2 = new Die();

        this.closedTiles = new boolean[12];
    }

    public String toString() {
        String s = this.closedTiles[0] ? "X" : "1";
        for (int i = 1; i < this.closedTiles.length; i++) {
            s += " " + (this.closedTiles[i] ? "X" : Integer.toString(i + 1));
        }
        return s;
    }

    public boolean playATurn() {
        this.die1.roll();
        this.die2.roll();
        System.out.println(this.die1 + ", " + this.die2);
        int sum = this.die1.getPips() + this.die2.getPips();
        if (this.closedTiles[sum - 1]) {
            System.out.println("Tile " + sum + " is Already Shut!");
            return true;
        } else {
            this.closedTiles[sum - 1] = true;
            System.out.println("Closing tile: " + sum);
            return false;
        }
    }

}
