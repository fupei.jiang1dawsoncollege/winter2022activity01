import java.util.Scanner;

public class MyInputUtil {

    private static Scanner myScanner = new Scanner(System.in); // Create a Scanner object

    // EXAMPLE USAGE:
    // if (MyInputUtil.inputY_N_withDefault("do you wish to continue ? (Y/n)", true)) {
    //     continue;
    // }
    // break;
    public static boolean inputY_N_withDefault(String prompt, boolean defaultBool) {

        String input;
        while (true) {
            System.out.print(prompt);
            input = myScanner.nextLine(); // Read user input

            //when user simply presses {Enter}
            if (input.length() == 0) {
                return defaultBool;
            }

            // must have len now
            switch (Character.toLowerCase(input.charAt(0))) {
                case 'y':
                    return true;
                case 'n':
                    return false;
            }

        }
    }

    //pretty useless
    public static String inputStr(String prompt) {
        System.out.print(prompt);
        return myScanner.nextLine(); // Read user input
    }

    // trying to simulate string literal type ? https://www.typescriptlang.org/docs/handbook/release-notes/typescript-1-8.html#string-literal-types
    // EXAMPLE USAGE:
    // char whichCountry = inputChar("What country will you be visiting?", "'c' for Canada, 'u' for United States, 'f' for France");
    public static char inputChar(String prompt, String charChoices) {
        System.out.println(prompt + " " + charChoices);

        java.util.Set<Character> possibleChars = new java.util.HashSet<Character>();

        int charChoicesLen = charChoices.length() - 1; // we don't want ' if it's last char (there's nothing after it)
        for (int c = 0; c < charChoicesLen; c++) {
            if (charChoices.charAt(c) == '\'') { // '
                c++; // since we are taking next char, we can skip checking next char
                possibleChars.add(charChoices.charAt(c)); // LOL, it works, char -> Character
                c++; // we can also skip '
            }
        }
        // System.out.println(possibleChars);

        String input;
        while (true) {
            input = myScanner.nextLine().trim(); // Read user input
            int len = input.length();

            if (len == 0) {
                System.out.println("you need to input something");
                continue;
            }

            //I decide to take the 1st character, so we're ignoring this
            // if (len != 1) {
            // System.out.println("you entered " + len + " characters, you need to enter 1 character");
            // continue;
            // }

            // must have len now
            char loweredChar = Character.toLowerCase(input.charAt(0));
            if (!possibleChars.contains(loweredChar)) {
                System.out.println(charChoices);
                continue;
            }

            return loweredChar;

        }
    }

    //useful for age
    public static int inputIntegerBiggerThanZero(String prompt) {
        System.out.print(prompt);
        double parsedNum;
        while (true) {
            String input = myScanner.nextLine(); // Read user input
            try {
                parsedNum = Double.parseDouble(input);
            } catch (NumberFormatException e) {
                System.out.println("you must enter a NUMBER");
                continue;
            }
            if (!(parsedNum > 0)) {
                System.out.println("your number must be >0 (bigger than 0)");
                continue;
            }

            int intNum = (int) parsedNum;
            if (parsedNum != intNum) {
                if (parsedNum > Integer.MAX_VALUE) {
                    System.out.println("your number must be smaller or equal to 2147483647");
                } else {
                    System.out.println("you must enter an INTEGER (without decimals)");
                }
                continue;
            }
            return intNum;
        }

    }

    //useful for width of rectangle
    public static double inputPositiveDouble(String prompt) {
        System.out.print(prompt);
        double parsedNum;
        while (true) {
            String input = myScanner.nextLine(); // Read user input
            try {
                parsedNum = Double.parseDouble(input);
            } catch (NumberFormatException e) {
                System.out.println("you must enter a NUMBER");
                continue;
            }
            if (parsedNum < 0) {
                System.out.println("your number must be POSITIVE");
                continue;
            }
            return parsedNum;
        }

    }

    //useful for length of a song
    public static int inputSeconds(String prompt) {
        System.out.println(prompt + " Ex: enter 2:10 for 2minutes 10seconds, or 180 for 180seconds");
        while (true) {
            String input = myScanner.nextLine(); // Read user input

            int pos_colon = input.indexOf(':');
            int secondsToReturn;
            try {
                if (pos_colon == -1) {
                    secondsToReturn = Integer.parseInt(input.trim());
                } else {

                    String hours_Str = input.substring(0, pos_colon).trim();
                    String minutes_Str = input.substring(pos_colon + 1).trim();

                    int hours_int = Integer.parseInt(hours_Str);
                    int minutes_int = Integer.parseInt(minutes_Str);

                    secondsToReturn = hours_int * 60 + minutes_int;
                }
            } catch (NumberFormatException e) {
                System.out.println("you must enter a NUMBER");
                continue;
            }

            // System.out.println(secondsToReturn + " seconds");
            return secondsToReturn;
        }

    }

}
