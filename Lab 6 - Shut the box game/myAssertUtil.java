import java.lang.reflect.Field;

public class myAssertUtil {

    public static void setPrivateField(Object obj, String fieldName, Object value) {
        // https://www.geeksforgeeks.org/how-to-access-private-field-and-method-using-reflection-in-java/
        try {
            Field privateField = obj.getClass().getDeclaredField(fieldName);
            privateField.setAccessible(true);
            privateField.set(obj, value);
        } catch (NoSuchFieldException e) {
            printErrorAtLine("NoSuchField \"" + escapeString(fieldName) + "\" class " + obj.getClass().getName());
        } catch (IllegalAccessException e) {
            printErrorAtLine("IllegalAccessExceptionJava, I don't know how to handle this");
        }

    }

    private static void printErrorAtLine(String errorMessage) {
        StackTraceElement _stackTraceObj = getStackTraceObj(2);
        System.out.println(
                "++++++++++++++++++++\n" + _stackTraceObj.getFileName() + ":" + _stackTraceObj.getLineNumber() + "\n" +
                        errorMessage
                        + "\n--------------------");
    }

    public static void assert_String(Object got_obj) {
        System.out.println(got_obj);
    }

    public static void assert_String(Object got_obj, Object want_obj) {
        String got = got_obj.toString();
        String want = want_obj.toString();
        if (!got.equals(want)) {
            String got_obj_className = got_obj.getClass().getName();
            String want_obj_className = want_obj.getClass().getName();

            printErrorAtLine(" got: \"" + escapeString(got) + "\""
            + (got_obj_className.equals("java.lang.String") ? "" : " class " + got_obj_className)
            + "\nwant: \"" + escapeString(want) + "\""
            + (want_obj_className.equals("java.lang.String") ? "" : " class " + want_obj_className));
            //error at line 3
            // got: "123456: Swetha, Computer Science" class Student
            //want: "Swetha: 123456, Computer Science"
            //error at line 4
            // got: "123456: Swetha, Computer Science" class Student
            //want: "Swetha: 123456, Computer Science" class Student
            //error at line 5
            // got: "123456: Swetha, Computer Science"
            //want: "Swetha: 123456, Computer Science"
        }
    }

    /**
    * escapeString()
    *
    * Escape a give String to make it safe to be printed or stored.
    *
    * @param s The input String.
    * @return The output String.
    **/
    private static String escapeString(String s) { // https://stackoverflow.com/questions/2406121/how-do-i-escape-a-string-in-java#61628600
        return s.replace("\\", "\\\\")
                .replace("\t", "\\t")
                .replace("\b", "\\b")
                .replace("\n", "\\n")
                .replace("\r", "\\r")
                .replace("\f", "\\f")
                .replace("\'", "\\'")
                .replace("\"", "\\\"");
    }

    private static StackTraceElement getStackTraceObj(int depthUp) {
        return Thread.currentThread().getStackTrace()[depthUp + 2];
        // getStackTraceObj(1).getLineNumber();
        // getStackTraceObj(1).getFileName();
    }
}
