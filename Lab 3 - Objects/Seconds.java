public class Seconds {

    private static String formatSeconds_To_SomethingSpaceSomething(int seconds, String something1, String something2) {
        return formatSeconds_To_SomethingSpaceSomething(seconds, something1, something2, "");
    }
    private static String formatSeconds_To_SomethingSpaceSomething(int seconds, String something1, String something2, String plural) {
        int howManyMinutes = seconds / 60;
        int howManySeconds = seconds - howManyMinutes * 60;


        String stringToReturn = "";

        if (howManyMinutes == 1) {
            stringToReturn+=howManyMinutes + something1;
        } else if (howManyMinutes > 1) {
            stringToReturn+=howManyMinutes + something1 + plural;
        }

        if (howManySeconds == 1) {
            stringToReturn+=howManySeconds + something2;
        } else {
            //yes, I want 0. 0seconds or 2 seconds or more
            stringToReturn+=howManySeconds + something2 + plural;
        }

        // 0seconds
        // 2seconds
        // 1minute 0seconds
        // 1minute 2seconds
        // 2minutes 0seconds
        // 2minutes 2seconds
        return stringToReturn;
    }

    public static String formatSeconds_To_MinSpaceS(int seconds) {
        return formatSeconds_To_SomethingSpaceSomething(seconds, "min", "s");
    }

    public static String formatSeconds_To_MinutesSpaceSeconds(int seconds) {
        return formatSeconds_To_SomethingSpaceSomething(seconds, "minute", "second", "s");
    }

    //------

    public static String formatSeconds_To_MinutesColonSeconds(int seconds) {
        int howManyHours = seconds / 60;
        int howManySeconds = seconds - howManyHours * 60;

        return howManyHours + ":" + howManySeconds;
    }
}
