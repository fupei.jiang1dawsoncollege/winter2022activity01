public class Shop {

    public static void main(String[] args) {
        Song[] songArr = new Song[4];
        for (int i = 0; i < 4; i++) {
            songArr[i] = new Song();
            songArr[i].name = MyInputUtil.inputStr("what's the name of the Song ?\n");
            songArr[i].lengthInSeconds = MyInputUtil.inputSeconds("what's the length ? "); // 3:32
            songArr[i].ratingOnFive = MyInputUtil.inputIntegerBiggerThanZero("what's the rating on 5 ? (?/5)\n");
            System.out.println("");
        }

        //test
        // songArr[3] = new Song();
        // songArr[3].name = "Never Gonna Give You Up";
        // songArr[3].lengthInSeconds = 3*60+32; // 3:32
        // songArr[3].ratingOnFive = 5;

        System.out.println(songArr[3].name);
        System.out.println(songArr[3].lengthInSeconds);
        System.out.println(songArr[3].ratingOnFive);

        songArr[3].play();

    }

}