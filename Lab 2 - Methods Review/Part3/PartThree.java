import java.util.Scanner;

class PartThree {
    private static Scanner myScanner;
    public static void main(String[] args) {
        myScanner = new Scanner(System.in); // Create a Scanner object

        double squareLength = inputPositiveDouble("Enter length of square: ");
        double rectangleLength = inputPositiveDouble("Enter length of rectangle: ");
        double rectangleWidth = inputPositiveDouble("Enter width of rectangle: ");

        System.out.println(AreaComputations.areaSquare(squareLength));
        AreaComputations instance_area_computations = new AreaComputations();
        System.out.println(instance_area_computations.areaRectangle(rectangleLength, rectangleWidth));

    }

    public static double inputPositiveDouble(String prompt) {
        System.out.print(prompt);
        double parsedNum;
        while (true) {
          String input = myScanner.nextLine(); // Read user input
          try {
            parsedNum = Double.parseDouble(input);
          } catch (NumberFormatException e) {
            System.out.println("you must enter a NUMBER");
            continue;
          }
          if (parsedNum < 0) {
            System.out.println("your number must be POSITIVE");
            continue;
          }
          return parsedNum;
        }

      }
}
