class MethodsTest {
    public static void main(String[] args) {
        int x = 10;
        methodOneInputNoReturn(10);
        methodOneInputNoReturn(x);
        methodOneInputNoReturn(x+50);


        methodTwoInputNoReturn(x, 3.14);

        int z = methodNoInputReturnInt();
        System.out.println(z);

        double c = sumSquareRoot(6, 3);
        System.out.println(c);


        String s1 = "hello";
        String s2 = "goodbye";
        System.out.println(s1.length());
        System.out.println(s2.length());

        System.out.println(SecondClass.addOne(50));
        SecondClass sc = new SecondClass();
        System.out.println(sc.addTwo(50));

        // System.out.println(x);
        // methodNoInputNoReturn();
        // System.out.println(x);
    }

    public static void methodNoInputNoReturn() {
        int x = 50;
        System.out.println(x);
        System.out.println("I'm in a method that takes no input and returns nothing");
    }

    public static void methodOneInputNoReturn(int youCanCallThisWhateverYouLike) {
        System.out.println("Inside the method one input no return");
        System.out.println(youCanCallThisWhateverYouLike);
    }

    public static void methodTwoInputNoReturn(int anInt, double aDouble) {
        System.out.println("Inside the method two inputs no return");
        System.out.println(anInt);
        System.out.println(aDouble);
    }

    public static int methodNoInputReturnInt() {
        return 6;
    }

    public static double sumSquareRoot(int a, int b) {
        int c = a + b;
        return Math.sqrt(c);
    }

}