import java.util.Scanner;

class BirthdayTrick {
    private static Scanner myScanner;

	public static void main(String[] args) {
		int birthMonth, birthDay;

        myScanner = new Scanner(System.in); // Create a Scanner object

        birthMonth = inputIntegerBiggerThanZero("input your month of birth (number): ");
        birthDay = inputIntegerBiggerThanZero("input your day of birth (number): ");

        double magicNumber = 7;
        magicNumber*=birthMonth;
        magicNumber-=1;
        magicNumber*=13;
        magicNumber+=birthDay;
        magicNumber+=3;
        magicNumber*=11;
        magicNumber-=birthMonth;
        magicNumber-=birthDay;
        magicNumber/=10;
        magicNumber+=11;
        magicNumber/=100;

        System.out.println(magicNumber);
	}

	public static int inputIntegerBiggerThanZero(String prompt) {
        System.out.print(prompt);
        double parsedNum;
        while (true) {
            String input = myScanner.nextLine(); // Read user input
            try {
                parsedNum = Double.parseDouble(input);
            } catch (NumberFormatException e) {
                System.out.println("you must enter a NUMBER");
                continue;
            }
            if (!(parsedNum > 0)) {
                System.out.println("your number must be >0 (bigger than 0)");
                continue;
            }

            int intNum = (int) parsedNum;
            if (parsedNum != intNum) {
                if (parsedNum > Integer.MAX_VALUE) {
                    System.out.println("your number must be smaller or equal to 2147483647");
                } else {
                    System.out.println("you must enter an INTEGER (without decimals)");
                }
                continue;
            }
            return intNum;
        }

    }
}