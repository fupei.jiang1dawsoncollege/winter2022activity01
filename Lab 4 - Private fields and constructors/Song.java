public class Song {
    private String name;
    private int lengthInSeconds;
    private int ratingOnFive;

    public Song(String name, int lengthInSeconds, int ratingOnFive){
        this.name = name;
        this.lengthInSeconds = lengthInSeconds;
        this.ratingOnFive = ratingOnFive;
    }

    //getters start
    public String getName() {
        return this.name;
    }

    public int getLengthInSeconds() {
        return this.lengthInSeconds;
    }

    public int getRatingOnFive() {
        return this.ratingOnFive;
    }
    //getters end

    //setters start
    public void setName(String newName) {
        this.name = newName;
    }

    public void setRatingOnFive(int newRatingOnFive) {
        this.ratingOnFive = newRatingOnFive;
    }
    //setters end

    // 5/5 : simply the best
    // 4/5 : good
    // 3/5 : just ok
    // 2/5 : bad
    // 1/5 : horrible

    // "??%?@$%?#??@?@?"
    private String getRatingStr() {
        String ratingStr;
        switch (this.ratingOnFive) {
            case 5:
                ratingStr = "simply the best";
                break;
            case 4:
                ratingStr = "good";
                break;
            case 3:
                ratingStr = "just ok";
                break;
            case 2:
                ratingStr = "bad";
                break;
            case 1:
                ratingStr = "horrible";
                break;
            default:
                ratingStr = "??%?@$%?#??@?@?";
        }
        return ratingStr;
    }

    // "Never Gonna Give You Up" is now playing...
    // it will be playing for 3min32s
    // you are now listening to a 5/5 rated song, throw away your personal biases, this song is simply the best.
    public void play() {
        System.out.println("\"" + this.name + "\" is now playing...");
        System.out.println("it will be playing for " + Seconds.formatSeconds_To_MinSpaceS(this.lengthInSeconds));
        System.out.println("you are now listening to a " + this.ratingOnFive
                + "/5 rated song, throw away your personal biases, this song is " + this.getRatingStr() + ".");
    }

}
