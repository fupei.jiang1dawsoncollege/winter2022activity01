public class Shop {

    public static void main(String[] args) {
        Song[] songArr = new Song[4];
        for (int i = 0; i < 4; i++) {
            songArr[i] = new Song(
                    MyInputUtil.inputStr("what's the name of the Song ?\n"),
                    MyInputUtil.inputSeconds("what's the length ? "), // 3:32
                    MyInputUtil.inputIntegerBiggerThanZero("what's the rating on 5 ? (?/5)\n"));
            System.out.println("");
        }

        //test
        // songArr[3] = new Song(
        // "Never Gonna Give You Up",
        // 3*60+32, // 3:32
        // 5
        // );

        // here, 3 == songArr.length - 1
        System.out.println("");
        System.out.println(songArr[songArr.length - 1].getName());
        System.out.println(songArr[songArr.length - 1].getLengthInSeconds());
        System.out.println(songArr[songArr.length - 1].getRatingOnFive());
        System.out.println("");

        songArr[songArr.length - 1] = new Song(
                MyInputUtil.inputStr("what's the name of the Song ?\n"),
                MyInputUtil.inputSeconds("what's the length ? "), // 3:32
                MyInputUtil.inputIntegerBiggerThanZero("what's the rating on 5 ? (?/5)\n"));

        System.out.println("");
        System.out.println(songArr[songArr.length - 1].getName());
        System.out.println(songArr[songArr.length - 1].getLengthInSeconds());
        System.out.println(songArr[songArr.length - 1].getRatingOnFive());
        System.out.println("");

        songArr[songArr.length - 1].play();

    }

}